//
//  Score.swift
//  AlexanderJerad_AdaptiveLayout
//
//  Created by Jerad Alexander on 11/21/19.
//  Copyright © 2019 Jerad Alexander. All rights reserved.
//

import Foundation

class Score
{
    //stored Properties
    var userName : String
    var score : Int
    
    //initializer
    init(name: String, score: Int)
    {
        self.userName = name
        self.score = score
    }
}
