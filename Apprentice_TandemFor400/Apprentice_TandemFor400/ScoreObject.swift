//
//  ScoreObject.swift
//  Apprentice_TandemFor400
//
//  Created by Jerad Alexander on 10/26/20.
//

import Foundation
class ScoreObject
{
    //stored Properties
    var userName : String
    var score : Int
    
    //initializer
    init(name: String, score: Int)
    {
        self.userName = name
        self.score = score
    }
}
