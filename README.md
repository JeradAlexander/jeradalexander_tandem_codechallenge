# README #

This is my Trivia game Submission for the Apprenticeship at Tandem


### Screenshots

![screen\s](images/screenshots.png)

### Intructions

Open this project with xcode and run the application on either a simulator or connected device running iOS 14.0

**Platform/IDE**

Xcode 12.0

**Test/Run**

* Iphone 11 Pro Max(actual device)
* Iphone 11 Pro(simulator)


