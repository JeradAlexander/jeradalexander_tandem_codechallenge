//
//  GameViewController.swift
//  Apprentice_TandemFor400
//
//  Created by Jerad Alexander on 10/26/20.
//

import UIKit
import CoreData

class GameViewController: UIViewController, UITextFieldDelegate {
    
    //Outlets
    @IBOutlet var playNextButton: UIButton!
    @IBOutlet var GameLabel: UILabel!
    @IBOutlet var answeredCorrectlyLabel: UILabel!
    @IBOutlet var answer1: UIButton!
    @IBOutlet var answer2: UIButton!
    @IBOutlet var answer3: UIButton!
    @IBOutlet var answer4: UIButton!
    
    //variables for logic
    var allTriviaQuestions = [QuestionObject]()
    var nameIsBlank = false
    var userName = ""
    var gameQuestions = [QuestionObject]()
    var tag = 10
    var currentQuestion = 0
    var correctAnswer = ""
    var answeredCorrectly = 0
    
    //coredata setup
    private var managedContext : NSManagedObjectContext!
    private var entityDescription : NSEntityDescription!
    private var scores : NSManagedObject!
    var leaderBoardScores = [ScoreObject]()
    var dataScores = [NSManagedObject]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //parsing json Data method
        parseJsondData()
        
        //calling name creator method
        DispatchQueue.main.async {
            self.namecreator()
        }
        
        //loading leaderboard
        loadLeaderBoard()
    }
    
    //MARK: method for when play or next is pressed
    @IBAction func PlayPressed(_ sender: Any) {
        
        //conditional for play/next button
        if playNextButton.titleLabel?.text == "Play" || playNextButton.titleLabel?.text == "Play Again"{
            //game setup
            gameQuestions = pickQuestions()
            currentQuestion = 0
            answeredCorrectly = 0
            answeredCorrectlyLabel.text = ""
            resetAnswerButtons()
            
            //changing button name
            playNextButton.setTitle("Next", for: .normal)
            
            //showing question
            GameLabel.text = (currentQuestion + 1).description + ". " +  gameQuestions[currentQuestion].question
            //setting correctanswer
            correctAnswer = gameQuestions[currentQuestion].correctAnswer
            
            //logic for answer buttons
            tag = 10
            for answer in gameQuestions[currentQuestion].answers.shuffled(){
                //switch to set titel of buttons by tags
                switch tag {
                case 10:
                    answer1.setTitle(answer, for: .normal)
                case 20:
                    answer2.setTitle(answer, for: .normal)
                case 30:
                    answer3.setTitle(answer, for: .normal)
                case 40:
                    answer4.setTitle(answer, for: .normal)
                default:
                    print("Shouldnt happen")
                }
                tag += 10
            }
            
        }else if playNextButton.titleLabel?.text == "Next" && currentQuestion < 10{
            //reseting answers
            resetAnswerButtons()
            
            //displaying question
            GameLabel.text = (currentQuestion + 1).description + ". " +  gameQuestions[currentQuestion].question
            //setting answer
            correctAnswer = gameQuestions[currentQuestion].correctAnswer
            
            //logic for answer buttons
            tag = 10
            for answer in gameQuestions[currentQuestion].answers.shuffled(){
                
                switch tag {
                case 10:
                    answer1.setTitle(answer, for: .normal)
                case 20:
                    answer2.setTitle(answer, for: .normal)
                case 30:
                    answer3.setTitle(answer, for: .normal)
                case 40:
                    answer4.setTitle(answer, for: .normal)
                default:
                    print("Shouldnt happen")
                }
                tag += 10
            }
        }
    }

    //logic for when a answer is selected
    @IBAction func answerPressed(_ sender: UIButton) {
        
        //setting to disabled until next is pressed
        answer1.isEnabled = false
        answer2.isEnabled = false
        answer3.isEnabled = false
        answer4.isEnabled = false
        //getting the selected answer
        let selectedanswer = sender.titleLabel?.text
        
        //condtional for if answer selected is correct answer
        if selectedanswer == correctAnswer{
            //correct answer lgic
            sender.backgroundColor = .green
            answeredCorrectly += 1
            answeredCorrectlyLabel.text = answeredCorrectly.description + " Answered Correctly"
        }else{
            //incorrect answer logic
            sender.backgroundColor = .red
            answeredCorrectlyLabel.text = answeredCorrectly.description + " Answered Correctly"
        }
        //incrementing current question
        currentQuestion += 1
        
        //logic for end of game and reset
        if currentQuestion == 10{
            answeredCorrectlyLabel.text = "Game Over\n You answered " +   answeredCorrectly.description + " out of " + gameQuestions.count.description + " correctly"
            playNextButton.setTitle("Play Again", for: .normal)
            saveData(score: ScoreObject(name: userName, score: answeredCorrectly))
        }
    }
    
    //MARK: Picking and shuffling questions
    func pickQuestions() -> [QuestionObject]
    {
        //emptying game cards
        var newGameQuestions = [QuestionObject]()
        //variable to stop getting multiples
        var alreadyChosen = [Int]()
        var x : Int = 0
        //loop while count is a quarter of view count
        while x < 10
        {
            //getting random int from cardsarray
            let rand  =  Int.random(in: 0...allTriviaQuestions.count - 1)
            
            //conditional for already chosen ints
            if alreadyChosen.contains(rand){
                
            }else
            {
                //adding int to chosen array
                alreadyChosen.append(rand)
                //setting card variable
                newGameQuestions.append(allTriviaQuestions[rand])
                
                x += 1
            }
        }
        //shuffling array
        newGameQuestions.shuffle()
        //returning gamecards
        return newGameQuestions
    }
    
    //method resets buttons
    func resetAnswerButtons(){
        answer1.isHidden = false
        answer2.isHidden = false
        answer3.isHidden = false
        answer4.isHidden = false
        answer1.isEnabled = true
        answer2.isEnabled = true
        answer3.isEnabled = true
        answer4.isEnabled = true
        answer1.backgroundColor = UIColor(named: "answerBack")
        answer2.backgroundColor = UIColor(named: "answerBack")
        answer3.backgroundColor = UIColor(named: "answerBack")
        answer4.backgroundColor = UIColor(named: "answerBack")
    }
    
    //MARK: Parsing Json Method
    func parseJsondData(){
        //getting path of json file
        if let path = Bundle.main.path(forResource: "Apprentice_TandemFor400_Data", ofType: ".json")
        {
            //converting to url
            let url = URL(fileURLWithPath: path)
            
            
            //converting binary to json do catch
            do
                   {
                    let data = try Data.init(contentsOf: url)
                    let questionsJson = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [Any]
                    
                    guard let triviaArray = questionsJson as? [[String:Any]]else {
                        print("Failed") ; return
                    }
                    
                    
                    //Looping through trivia objects
                    for trivia in triviaArray{
                        
                        //getting trivia data from json object
                        guard let question = trivia["question"] as? String, var answers = trivia["incorrect"] as? [String], let correct = trivia["correct"] as? String
                        else{
                            print("Failed parsing");return
                        }
                        //adding correct answer to answers array
                        answers.append(correct)
                        
                        //Creating Trivia Question object and adding to trivia questions list tto use for the trivia game later
                        allTriviaQuestions.append(QuestionObject(question: question, answers: answers, correctAnswer: correct))
                        
                    }
                   }
            catch
            { print(error.localizedDescription)
            }
        }
    }
    
    //MARK:leaderboard logic
    func loadLeaderBoard()
    {
        //setting app delegate
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        //setting context from persistant container
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        //2 fet request from entity
        let fetchRequest =
            NSFetchRequest<NSManagedObject>(entityName: "Scores")
        
        //3 completing the load
        do {
            dataScores = try managedContext.fetch(fetchRequest)
        } catch let error as NSError {
            print("Could not fetch. \(error), \(error.userInfo)")
        }
        //looping through data scores to add to leaderboar scores array
        for sc in dataScores
        {
            let nm : String =  sc.value(forKey: "userName") as! String
            let score : Int = sc.value(forKey: "score") as! Int
            
            //adding to leaderscore array
            leaderBoardScores.append(ScoreObject(name: nm, score: score))
        }
    }
    
    //saving score
    func saveData(score: ScoreObject)
    {
        guard let appDelegate =
                UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        
        // 1 manage context
        let managedContext =
            appDelegate.persistentContainer.viewContext
        
        // 2 entity
        let entity =
            NSEntityDescription.entity(forEntityName: "Scores",
                                       in: managedContext)!
        //object to save
        let scoretoSave = NSManagedObject(entity: entity,
                                          insertInto: managedContext)
        
        // 3 setting attributes values for keys
        scoretoSave.setValue(score.userName, forKey: "userName")
        scoretoSave.setValue(score.score, forKey: "score")
        
        // 4
        do {
            //saving item
            try managedContext.save()
            dataScores.append(scoretoSave)
            leaderBoardScores.append(score)
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    @IBAction func toLeaderBoardPressed(_ sender: Any) {
        performSegue(withIdentifier: "toLeader", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sc = segue.destination as? LeaderBoardViewController
        {
            //setting array on leaderboard viewcontroller
            sc.topScores = leaderBoardScores
        }
    }
}

extension GameViewController{
    //Method secondary actions
    
    @IBAction func ChangeNamePressed(_ sender: Any) {
        namecreator()
    }
    
    //MARK: creating and changing user name
    func namecreator()
    {
        if nameIsBlank == true
        {
            let alert = UIAlertController(title: "Your Name", message: "Player name cannot be left blank", preferredStyle: .alert)
            
            // Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                textField.text = ""
            }
            
            // Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak alert] (_) in
                if let textField = alert?.textFields![0]
                {
                    if textField.text == ""
                    {
                        textField.delegate = self
                        self.nameIsBlank = true
                        self.namecreator()
                    }else
                    {
                        textField.delegate = self
                        self.userName = textField.text!
                        self.nameIsBlank = false
                    }
                }
            }))
            //Present the alert.
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let alert = UIAlertController(title: "Welcome", message: "Please enter your name", preferredStyle: .alert)
            
            // Add the text field. You can configure it however you need.
            alert.addTextField { (textField) in
                textField.text = ""
            }
            // Grab the value from the text field, and print it when the user clicks OK.
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [self, weak alert] (_) in
                if let textField = alert?.textFields![0]
                {
                    if textField.text == ""
                    {
                        textField.delegate = self
                        self.nameIsBlank = true
                        self.namecreator()
                    }else
                    {
                        textField.delegate = self
                        self.userName = textField.text!
                        //only changes label if game has not been started
                        if playNextButton.titleLabel?.text == "Play"{
                            self.GameLabel.text = "Welcome to Tandem Trivia, " + self.userName
                        }
                       //changing boolean back
                        self.nameIsBlank = false
                    }
                }
            }))
            //Present the alert.
            self.present(alert, animated: true, completion: nil)
        }
    }
}
