//
//  QuestionObject.swift
//  Apprentice_TandemFor400
//
//  Created by Jerad Alexander on 10/26/20.
//

import Foundation

class QuestionObject
{
    //class variables
    var question : String
    var answers : [String]
    var correctAnswer : String
    
    //Constructor/initializer
    init(question: String, answers: [String], correctAnswer: String)
    {
        self.question = question
        self.answers = answers
        self.correctAnswer = correctAnswer
    }
}
