//
//  LeaderBoardViewController.swift
//  Apprentice_TandemFor400
//
//  Created by Jerad Alexander on 10/26/20.
//

import UIKit

class LeaderBoardViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    //outlets and variables
    @IBOutlet weak var ScoreTV: UITextView!
    @IBOutlet var scoresTableView: UITableView!
    
    //variables for leaderboard
    var topScores = [ScoreObject]()
    var sortedScores = [ScoreObject]()
    var place : Int = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //sorting cores array
        sortedScores = topScores.sorted(by: { $0.score > $1.score })// For ascending sort
        
        //setting tableview delegate
        scoresTableView.delegate = self
        scoresTableView.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //scored count
        sortedScores.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //setting cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "score_cell", for: indexPath)
        
        // setting up cell
        cell.textLabel?.text = sortedScores[indexPath.row].userName
        cell.detailTextLabel?.text = sortedScores[indexPath.row].score.description
        
        return cell
    }
    
    //dismissing view
    @IBAction func exitLeader(_ sender: UIButton)
    {
        dismiss(animated: true, completion: nil)
    }
}
